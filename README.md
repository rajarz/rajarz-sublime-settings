# Rahul's Sublime Text Setup


##Favourite Keyboard Shortcuts

Command | Mac | Windows
--- | --- | ---
Toggle side bar | <kbd>Cmd</kbd> + <kbd>KB</kbd> | <kbd>Ctrl</kbd> + <kbd>KB</kbd>
Split view 2 cols | <kbd>Cmd</kbd> + <kbd>Alt</kbd> + <kbd>2</kbd> | 
Multi Selection (Add) | <kbd>Cmd</kbd> + <kbd>D</kbd>
Multi Selection (Skip) | <kbd>Cmd</kbd> + <kbd>KD</kbd>
Multi Selection (Down) | <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>Down</kbd>


## Installed Packages

Package | Rating | Link | About
--- | :---: | --- | --- 
Anaconda | :unamused: | http://damnwidget.github.io/anaconda/ | Python IDE plugin


##Required Software

* Node JS [v6.9.2](https://nodejs.org/dist/v6.9.2/node-v6.9.2-x64.msi)